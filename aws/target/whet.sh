#!/bin/sh -e
OUT=upload/whet.out
WHET=microbench/whet
NR=4000
TRY=16
while [ $TRY -gt 0 ] ; do
	$WHET $NR > "$OUT" 2>&1
	grep '^WARN: Insufficient duration' "$OUT"
	[ $? -eq 0 ] || break
	TRY=$(( $TRY - 1 ))
	NR=$(( NR << 1 ))
done
