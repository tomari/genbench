#!/bin/sh
SIZE=$1 # ref test
PROG=$2 # int fp

cp -a cpu2000.cfg cpu2000/config
cd cpu2000/
echo linux-glibc22-x86_64 | ./install.sh
source ./shrc
runspec -c cpu2000 -I --loose -o asc --tune=base --size=$SIZE $PROG

