#!/bin/sh -e
OUT=upload/dhry.out
DHRY=./microbench/dhry
NR=4000000
TRY=16
while [ $TRY -gt 0 ] ; do
	echo $NR | $DHRY > "$OUT"
	grep '^Please increase number of runs' "$OUT"
	[ $? -eq 0 ] || break
	TRY=$(( $TRY - 1 ))
	NR=$(( NR << 1 ))
done
