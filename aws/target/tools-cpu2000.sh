#!/bin/sh -e
HERE=$(realpath `dirname $0`)

## update config.sub to newer version
curl -s --compressed -o config.sub 'https://sourceware.org/git/gitweb.cgi?p=binutils-gdb.git;a=blob_plain;f=config.sub;hb=master'
find tools/src -type f -name 'config.sub' -exec cp config.sub {} \;
curl -s --compressed -o config.guess 'https://sourceware.org/git/gitweb.cgi?p=binutils-gdb.git;a=blob_plain;f=config.guess;hb=master'
find tools/src -type f -name 'config.guess' -exec cp config.guess {} \;

## patch buildtools sources
patch -p0 < "$HERE"/cpu2000-buildtools.patch

## do the build
export BZIP2CFLAGS=-fPIC
cd tools/src
./buildtools

