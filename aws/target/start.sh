#!/bin/sh
BUCKET=benchmarkheaven
NPBCLASS=S
#NPBCLASS=A
CPU200X_SIZE=test
CPU200X_PROG=int
AWS="aws"

# create upload staging directory
if [ ! -d upload ] ; then
	mkdir upload
fi
source ./hinv.sh

# run benchmarks
source ./calcmhz.sh
source ./npb.sh
source ./cpu2000.sh
source ./cpu2006.sh
source ./microbench.sh

# upload result
cp /var/log/cloud-init.log upload/
TARNAME=`hostname -s`_`date +%Y%m%d-%H%M%S`.tar.xz
tar cf - upload | xz -9 > "$TARNAME"
$AWS s3 cp "$TARNAME" "s3://$BUCKET/logs/$TARNAME"

