# Download source tarball
NPB_TAR="NPB3.3.1.tar.gz"
$AWS s3 cp "s3://$BUCKET/$NPB_TAR" "$NPB_TAR"
tar zxf "$NPB_TAR"

# patch DC lseek
patch -p0 < npb-fseeko.patch

# create config
cp -a make.def-ser NPB3.3.1/NPB3.3-SER/config/make.def
cp -a make.def-omp NPB3.3.1/NPB3.3-OMP/config/make.def

for i in SER OMP; do
	cat "NPB3.3.1/NPB3.3-${i}/config/suite.def.template" | \
	grep -v ^\# | grep -v ^$ | \
	sed "s/S/$NPBCLASS/" > \
	"NPB3.3.1/NPB3.3-${i}/config/suite.def"
done

# build
make -C NPB3.3.1/NPB3.3-SER suite &
make -C NPB3.3.1/NPB3.3-OMP suite &
wait

# run
export OMP_NUM_THREADS=$(nproc)
for j in 1 2 3 ; do
for i in OMP SER; do
	for b in NPB3.3.1/NPB3.3-${i}/bin/*.x; do
		./$b >> upload/npb.log
		rm -f ADC.*
	done
done
done

