#!/bin/sh -e
STREAM=microbench/stream
OUT=upload/stream.out
FREE_BYTES=$(cat /proc/meminfo |grep ^MemTotal: | awk '{print $2}')
USE_BYTES=$(( 1024 * $FREE_BYTES /3 ))
N=$(( $USE_BYTES/24 ))
export OMP_NUM_THREADS=`nproc`
$STREAM "$N" > "$OUT"

