# copy tarball
TAR_NAME=cpu2006-1.0.1.tar.xz

$AWS s3 cp "s3://$BUCKET/$TAR_NAME" "$TAR_NAME"

mkdir cpu2006
tar xJf "$TAR_NAME" -C cpu2006
chmod -R +w cpu2006

./run-cpu2006.sh $CPU200X_SIZE "$CPU200X_PROG" > upload/cpu2006.log 2>&1
cp -a cpu2006/result/*.txt upload/

