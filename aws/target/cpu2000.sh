# copy tarball
TAR_NAME=cpu2000-1.3.1.tar.xz

$AWS s3 cp "s3://$BUCKET/$TAR_NAME" "$TAR_NAME"

mkdir cpu2000
tar xJf "$TAR_NAME" -C cpu2000
chmod -R +w cpu2000

./run-cpu2000.sh $CPU200X_SIZE "$CPU200X_PROG" > upload/cpu2000.log 2>&1
cp -a cpu2000/result/*.asc upload/

