#!/usr/bin/perl
# NAS Parallel Benchmarks log analyzer. Version 1.0, 2012 H.Tomari. Public Domain.
#
# Reads NPB execution logs from STDIN and prints the summary to STDOUT in CSV format.
# * Logs of both serial and parallel implementations can be supplied at once.
#   Serial runs are handled as np=0
# * Logs of multiple runs of the same implementation and thread count can be supplied.
#   This program uses the median value for summary output (as in SPEC benchmarks).
# * If parameter 0 is passed to the script this program calculates the relative speedup
#   of benchmarks compared to the serial execution. Pass parameter `1' to select the
#   NP=1 run as the reference result.
#
# Usage example:
# # edit NPB3.3.1/NPB3.3-OMP/config/*.def
# $ make -C NPB3.3.1/NPB3.3-OMP suite
# $ for i in 1 2 3 ; do
# -   for p in 4 3 2 1; do
# -     for x in NPB3.3.1/NPB3.3-OMP/bin/*.x ; do
# -       echo RUN $i NP=$p $x;
# -       env OMP_NUM_THREADS=$p ./$x >> npb.log ; done ; done; done
# $ perl npbanal.pl < npb.log
#
# Output example (Sun Ultra Enterprise 3000, 6 US-II @ 336MHz, Solaris 10 u10ga2, Sun CC 5.12)
#   ,      BT.A,      CG.A,      EP.A,      FT.A,      LU.A,      MG.A,      SP.A,      UA.A,
#  1,    178.05,     53.04,      3.16,    109.06,    173.22,     90.69,     85.36,      0.49,
#  2,    355.13,    118.38,      6.43,    209.46,    341.15,    168.75,    166.61,      0.97,
#  3,    344.73,    176.33,      9.46,    303.52,    507.02,    237.95,    246.95,      1.46,
#  4,    700.09,    216.03,     12.61,    386.98,    647.62,    307.44,    310.14,      1.90,
#  5,    843.42,    253.19,     15.67,    460.26,    762.88,    359.43,    361.37,      2.18,
#  6,   1005.91,    296.34,     18.89,    528.66,    886.76,    400.94,    452.29,      2.57,
#
use strict;
#use Data::Dumper;

sub readlog {
  my @res;
  my $x;
  my $vers;
  my $np;
  my $cls;
  my $mops;
  my $ok;
  my $done=0;
  while(<STDIN>) {
    if($done==0) {
      if($_ =~ / NAS Parallel Benchmarks \(NPB[^)-]+-(\w+)\) - (\w\w) Benchmark/) {
	$vers=$1;
	$x=$2;
	$np=0;
	$cls=undef;
	$mops=undef;
	$ok=0;
	$done=0;
      } elsif ($_ =~ /^ (\w\w) Benchmark Completed/) {
	if($1 != $x ) {
	  print STDERR "oops, something is wrong!\n";
	}
	$done=1;
      }
    } else {
      if($_ =~ /\s*(.+)=\s*(.+)$/) {
	my $key=$1;
	my $val=$2;
	$key=~s/\s\s+//;
	$val=~s/\s\s+//;
	if($key=~/Class/) {
	  $cls=$val;
	} elsif ($key=~/Mop\/s total/) {
	  $mops=$val;
	} elsif ($key=~/Avail threads/) {
	  $np=$val;
	} elsif ($key=~/Verification/) {
	  $ok=1 if($val=~ /SUCCESSFUL/)
	}
      } else {
	my @row=($vers, $x.".".$cls, $np, $mops, $ok);
	push(@res,\@row);
	#print "Found $x result.\n";
	$done=0;
      }
    }
  }
  return \@res;
}

sub reduce {
  my %scores=%{$_[0]};
  foreach my $np (keys(%scores)) {
    my %npl=%{$scores{$np}};
    foreach my $be (keys(%npl)) {
      my @sl=@{$npl{$be}};
      my @sorted=sort {$a<=>$b} @sl;
      $npl{$be}=$sorted[int($#sorted/2)];
    }
    $scores{$np}=\%npl;
  }
  return \%scores;
}

sub export_scores {
  my %scores=%{$_[0]};
  my @benches=@{$_[1]};
  my @nps=@{$_[2]};
  my $npfmt="%3s,";
  my $fldfmt="%10s,";
  my $newline="\n";
  printf STDOUT $npfmt,"";
  foreach my $bench (@benches) {
    printf STDOUT $fldfmt,$bench;
  }
  print STDOUT $newline;
  foreach my $np (@nps) {
    printf STDOUT $npfmt,$np;
    my %res=%{$scores{$np}};
    foreach my $bench (@benches) {
      printf STDOUT $fldfmt,$res{$bench};
    }
    print STDOUT $newline;
  }
}

sub calc_rel {
  my %scores=%{$_[0]};
  my $base=$_[1];
  my %baserow=%{$scores{$base}};
  foreach my $np (keys(%scores)) {
    if($np!=$base) {
      my %row=%{$scores{$np}};
      foreach my $bench (keys(%row)) {
	if(defined($row{$bench}) && defined($baserow{$bench}) && $baserow{$bench}>0) {
	  $row{$bench}=$row{$bench}/$baserow{$bench};
	} else {
	  $row{$bench}=undef;
	}
      }
      $scores{$np}=\%row;
    }
  }
  foreach my $bench (keys(%baserow)) {
    $baserow{$bench}=1. if(defined($baserow{$bench}));
  }
  $scores{$base}=\%baserow;
  return \%scores;
}

sub npbanalyze {
  my $ref=$_[0];
  my @res=@{readlog()};
  my %stat;
  for my $i (0..$#res) {
    my @rec=@{$res[$i]};
    my ($vers, $x, $np, $mops, $ok) = @rec;
    my %statrec;
    if(defined($stat{$np})) {
      %statrec=%{$stat{$np}};
    }
    push(@{$statrec{$x}},$mops) if $ok;
    $stat{$np}=\%statrec;
  }
  my @nps=sort {$a <=> $b} keys(%stat);
  my @benches;
  my %benchesh;
  for my $np (@nps) {
    for my $b (keys(%{$stat{$np}})) {
      $benchesh{$b}++;
    }
  }
  @benches=sort keys(%benchesh);
  my %scores;
  for my $npx (0..$#nps) {
    my %arr;
    foreach my $b (@benches) {
      my @t;
      $arr{$b}=\@t;
    }
    $scores{$nps[$npx]}=\%arr;
  }
  for my $i (0..$#res) {
    my @rec=@{$res[$i]};
    my ($vers, $x, $np, $mops, $ok) = @rec;
    my %npa=%{$scores{$np}};
    my @bena=@{$npa{$x}};
    push(@bena,$mops) if $ok;
    $npa{$x}=\@bena;
    $scores{$np}=\%npa;
  }
  my %mscores=%{reduce(\%scores)};
  my %rscores;
  if(defined($ref) && defined($mscores{0}) && $#nps>0) {
    %rscores=%{calc_rel(\%mscores,$ref)};
  } else {
    %rscores=%mscores;
  }
  export_scores(\%rscores,\@benches,\@nps);
}

if($#ARGV>=0) {
  npbanalyze($ARGV[$#ARGV]);
} else {
  npbanalyze(undef);
}
