#!/bin/sh
set -e

if [ "a$1" == "a" ] ; then
	echo Specify block device to erase
	exit 1;
fi

mkfs.ext4 -L work "$1"

if [ ! -d "/work" ]; then
	mkdir /work
fi
mount -t ext4 "$1" /work

chmod 777 /work

