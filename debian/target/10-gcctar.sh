#!/bin/sh
WORK=/work
GCC_TARBALL=debian9-gcc730.tar.xz

aws --region us-east-2 s3 cp "s3://benchmarkheaven/$GCC_TARBALL" "$WORK"/
tar xJf "$WORK"/"$GCC_TARBALL" -C /

