#!/bin/sh
set -e
WORK=/work
GCC_VER=7.3.0

GCC_DIR="$WORK/gcc"
GCC_SRC="$GCC_DIR/src"

if [ ! -d "$GCC_SRC" ] ; then
	mkdir -p "$GCC_SRC"
fi

cd "$GCC_SRC"

if [ ! -f "gcc-${GCC_VER}.tar.xz" ] ; then
wget -nd -nc "https://ftp.gnu.org/gnu/gcc/gcc-${GCC_VER}/gcc-${GCC_VER}.tar.xz"
tar xJf "gcc-${GCC_VER}.tar.xz"
fi

if [ -d "build" ] ; then
	rm -fr build
fi
mkdir build
cd build/

"../gcc-${GCC_VER}/configure" --prefix="${GCC_DIR}" --enable-languages=c,c++,fortran --disable-multilib --with-system-zlib --disable-libssp --disable-libsanitizer 2>&1 >> gcc-build.log
make -j$(nproc) 2>&1 >> gcc-build.log
make install 2>&1 >> gcc-build.log

cd /
rm -fr "$GCC_SRC"
tar cf - work/gcc | xz -9 -e > "$WORK"/debian9-gcc730.tar.xz
aws s3 cp "$WORK"/debian9-gcc730.tar.xz s3://benchmarkheaven/


