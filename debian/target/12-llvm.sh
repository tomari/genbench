#!/bin/bash
INSTALL_PREFIX=${HOME}/llvm
set -e
if [ ! -d llvm-project ] ; then
    git clone https://gitlab.com/freedesktop-sdk/mirrors/github/llvm/llvm-project.git
fi
pushd llvm-project
    git checkout llvmorg-10.0.0-rc5
popd
if [ -d build ] ; then
    rm -fr build
fi
mkdir build
pushd build
    cmake -DLLVM_ENABLE_PROJECTS="clang;libcxx;libcxxabi;openmp" -DCMAKE_CXX_STANDARD=17 -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="${INSTALL_PREFIX}" -DLLVM_TARGETS_TO_BUILD=X86 -G "Unix Makefiles" ../llvm-project/llvm
    make -j$(nproc)
    make install
popd

if [ ! -d f18 ] ; then
    git clone https://github.com/flang-compiler/f18.git
fi
export LLVM="${INSTALL_PREFIX}/lib/cmake/llvm"
if [ -d build-f18 ] ; then
    rm -fr build-f18
fi
mkdir build-f18
pushd build-f18
    cmake -DLLVM_DIR="$LLVM" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="$INSTALL_PREFIX" -DLINK_WITH_FIR=Off ../f18
    make -j2 # limit memory usage
    make install
popd
