#!/bin/sh
set -e

apt update
apt install -y --no-install-recommends \
	git \
	awscli \
	screen \
	bison \
	flex \
	autoconf \
	zlib1g-dev \
	libgmp-dev \
	libmpfr-dev \
	libmpc-dev \
	build-essential

echo "dash dash/sh boolean false" | debconf-set-selections
DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash

aws configure set region us-east-2

sysctl -w kernel.dmesg_restrict=0

