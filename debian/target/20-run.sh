#!/bin/sh
set -e
HERE=$(dirname $0)
WORK=/work

cp -a "$HERE"/../../aws/target "$WORK"
source ./15-gccenv.sh
cd "$WORK"/target
./start.sh

