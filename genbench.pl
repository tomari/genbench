#!/usr/bin/perl
use strict;
use warnings;
use List::Util qw(max);
use OpenOffice::OODoc;
use Storable qw(freeze);
use Digest::SHA qw(sha256_hex);
use JSON::PP;
my $descr= << "EOL"
genbench: reads an OpenDocument spreadsheet file to generate
JSON source for the Benchmark Heaven. 2012-2017 H.Tomari [Public Domain]
requires OpenOffice::OODoc
usage: perl genbench.pl bench.ods

EOL
    ;
$Storable::canonical=1;

sub get_titles {
  my ($doc, $sheet, $maxcol)=@_;
  my @res;
  my @row=$doc->getRowCells($sheet,0);
  for my $i (0 .. $maxcol-1) {
    my $t=$doc->getText($row[$i]);
    goto end if $t =~ /^\ *$/;
    push(@res,$t);
  }
 end:
  return \@res;
}

sub get_hashedrows {
  my ($doc, $sheet, $maxrow, $titles_)=@_;
  my @titles=@$titles_;
  my @res;
  for my $r (1 .. $maxrow-1) {
    my %h;
    my @arow=$doc->getRowCells($sheet,$r);
    for my $c (0 .. $#titles) {
      my $t=$doc->getText($arow[$c]);
      goto end if ($c==0 && $t =~ /^\ *$/);
      my $key=$titles[$c];
      $h{$titles[$c]}=$t;
    }
    push(@res,\%h);
  }
 end:
  return @res;
}

sub detect_size {
  my ($doc, $sheet)=@_;
  my ($nrow, $ncol)=$doc->getTableSize($sheet);

  my $realrow=16;
  my $t;
  do {
    $realrow=$realrow*2;
    $doc->normalizeSheet($sheet,$realrow,1);
    $t=$doc->getText($doc->getTableCell($sheet,$realrow,0));
  } until($realrow>=$nrow || $t =~ /^\ *$/);
  my $realcol=16;
  do {
    $realcol=$realcol*2;
    $doc->normalizeSheet($sheet,1,$realcol);
    $t=$doc->getText($doc->getTableCell($sheet,0,$realcol));
  } until($realcol>=$ncol || $t =~ /^\ *$/);

  print "This sheet is smaller than $realrow rows and $realcol columns.\n";
  $doc->normalizeSheet($sheet,$realrow,$realcol);
  return ($realrow,$realcol);
}

sub read_benchsrc {
  my $filename=shift;
  print "Source filename: $filename \n";
  my $oofile=odfContainer($filename);
  my $doc=odfDocument(container=>$oofile,part => 'content');

  my $tablenum=0;
  my $sheet=$doc->getTable($tablenum) or die("No sheet found.");
  my ($nrow, $ncol)=detect_size($doc,$sheet);
  my @titles=@{get_titles($doc,$sheet,$ncol)};
  my @raw=get_hashedrows($doc,$sheet,$nrow,\@titles);
  my $cnt=$#raw+1;
  print "Read $cnt machines.\n";
  return \@titles,\@raw;
}

sub categorize {
  my ($title_,$sheet_)=@_;
  my @title=@$title_;
  my @sheet=@$sheet_;
  my @cat;
  for my $i (0 .. $#title) {
    push(@cat,0);
  }
  # 0: empty 1: numeric 2: string
  for my $r (0 .. $#sheet) {
    my %rdata=%{$sheet[$r]};
    for my $c (0 .. $#title) {
      my $colhead=$title[$c];
      my $cell=$rdata{$colhead};
      my $thiscat;
      $cell =~ s/,//;
      if($cell =~ /^ \*/) {
	$thiscat=0;
      } elsif ($cell =~ /^-?\d*(\.\d*)?$/) {
	$thiscat=1;
      } else {
	$thiscat=2;
      }
      $cat[$c]=max($thiscat,$cat[$c]);
    }
  }
  my @mrec;
  my @brec;
  for my $c (0 .. $#title) {
    push(@mrec, $title[$c]) if ($cat[$c]==2);
    push(@brec, $title[$c]) if ($cat[$c]==1);
  }
  return (\@mrec, \@brec);
}

sub minify_strarr {
  my @a=@{$_[0]};
  my $i=$#a;
  while(!length($a[$i]) && $i) {$i--;}
  my @r=splice(@a,0,$i+1);
  return \@r;
}

sub minify_numarr {
  my @a=@{$_[0]};
  my $i=$#a;
  while($a[$i]<0 && $i) {$i--;}
  my @r=splice(@a,0,$i+1);
  return \@r;
}

sub fill_rows {
  my ($t_cols,$n_cols,$sheet)=@_;
  my %res;
  foreach my $row (@$sheet) {
    my @t_flds_long=map {$$row{$_}} @$t_cols;
    my @t_flds=@{minify_strarr(\@t_flds_long)};
    my $keyForRow=substr(sha256_hex(freeze \@t_flds),0,6); # scary
    my @n_flds_long=map {my $f=$$row{$_}; length($f)?$f+0.0:-1;} @$n_cols;
    my @n_flds=@{minify_numarr(\@n_flds_long)};
    $res{$keyForRow}={'t'=>\@t_flds,'n'=>\@n_flds};
  }
  return \%res;
}

my $numarg=$#ARGV+1;
if($numarg==1) {
  my ($title,$sheet)=read_benchsrc($ARGV[0]);
  my ($t_cols, $n_cols)=categorize($title,$sheet);
  my $body=fill_rows($t_cols,$n_cols,$sheet);
  my %json_src=('t_cols'=>$t_cols,'n_cols'=>$n_cols,'d'=>$body);
  open(my $json_h,'>','bench.json') or die "cannot open json for writing: $!";
  print $json_h JSON::PP->new->utf8(1)->pretty(1)->canonical(1)->indent_length(1)->encode(\%json_src);
  close $json_h;
} else {
  print STDERR $descr;
}

exit 0;
