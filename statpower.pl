#!/usr/bin/perl
my $descr= << "EOL"
preprocess Benchmark Heaven ODS file for gnuplotting Performance/Power-Year graph
usage: perl statpower.pl bench.ods
EOL
;
my $outfile_base="perfpwr";
use strict;
use OpenOffice::OODoc;

sub get_titles {
  my ($doc, $sheet, $maxcol)=@_;
  my @res;
  my @row=$doc->getRowCells($sheet,0);
  for my $i (0 .. $maxcol-1) {
    my $t=$doc->getText($row[$i]);
    goto end if $t =~ /^\ *$/;
    push(@res,$t);
  }
 end:
  return @res;
}

sub get_hashedrows {
  my ($doc, $sheet, $maxrow, $titles_)=@_;
  my @titles=@$titles_;
  my @res;
  for my $r (1 .. $maxrow-1) {
    my %h;
    my @arow=$doc->getRowCells($sheet,$r);
    for my $c (0 .. $#titles) {
      my $t=$doc->getText($arow[$c]);
      goto end if ($c==0 && $t =~ /^\ *$/);
      my $key=$titles[$c];
      $h{$titles[$c]}=$t;
    }
    push(@res,\%h);
  }
 end:
  return @res;
}

sub detect_size {
  my ($doc, $sheet)=@_;
  my ($nrow, $ncol)=$doc->getTableSize($sheet);

  my $realrow=16;
  my $t;
  do {
    $realrow=$realrow*2;
    $doc->normalizeSheet($sheet,$realrow,1);
    $t=$doc->getText($doc->getTableCell($sheet,$realrow,0));
  } until($realrow>=$nrow || $t =~ /^\ *$/);
  my $realcol=16;
  do {
    $realcol=$realcol*2;
    $doc->normalizeSheet($sheet,1,$realcol);
    $t=$doc->getText($doc->getTableCell($sheet,0,$realcol));
  } until($realcol>=$ncol || $t =~ /^\ *$/);

  print STDERR "Detected a sheet with no more than $realrow rows and $realcol columns.\n";
  $doc->normalizeSheet($sheet,$realrow,$realcol);
  return ($realrow,$realcol);
}

sub read_benchsrc {
  my $filename=shift;
  print STDERR "Source filename: $filename \n";
  my $oofile=odfContainer($filename);
  my $doc=odfDocument
    (
     container=>$oofile,
     part => 'content'
    );
  my $tablenum=0;
  my $sheet=$doc->getTable($tablenum) or die("No sheet found.");
  my ($nrow, $ncol)=detect_size($doc,$sheet);
  my @titles=get_titles($doc,$sheet,$ncol);
  my @raw=get_hashedrows($doc,$sheet,$nrow,\@titles);
  my $cnt=$#raw+1;
  print STDERR "Read $cnt machines.\n";
  return \@titles,\@raw;
}

sub datnameof {
  my $arch=shift;
  return $outfile_base."-".$arch.".dat"
}

sub genplot {
  #my $arch_=shift;
  my @arch=sort @_;
  my $gplfilename=$outfile_base.".gpl";
  print STDERR "generate $gplfilename\n";
  open(my $fh, ">", $gplfilename) or die "cannot open $gplfilename: $!";
  print $fh "plot";
  for my $i (0 .. $#arch) {
    $a=$arch[$i];
    print $fh "\t\"".datnameof($a)."\" u 2:3 title \"".$a."\", '' using 2:3:1 with labels notitle";
    if($i==$#arch) {
      print $fh "\n"
    } else {
      print $fh ", \\\n";
    }
  }
  close $fh;
}

sub datfy {
  my ($title_,$sheet_)=@_;
  my @title=@$title_;
  my @sheet=@$sheet_;
  my %archfh;
  for my $r (0 .. $#sheet) {
    my %rdata=%{$sheet[$r]};
    my $arch=lc($rdata{"Arch"});
    my $fh;
    if(!defined $archfh{$arch}) {
      my $filename=datnameof($arch);
      open($fh, ">", $filename) or die "cannot open $filename: $!";
      print STDERR "open ".$filename."\n";
      $archfh{$arch}=$fh;
    } else {
      $fh=$archfh{$arch};
    }
    my $bunbo=$rdata{"Prun[W]"};
    if((!defined $bunbo)||$bunbo==0) {
      #print STDERR "skip ".$rdata{"Machine"}."\n";
      next;
    }
    my $bunshi=$rdata{"DhrystoneMIPS"};
    if(!defined $bunshi) {
      #print STDERR "skip ".$rdata{"Machine"}."\n";
      next;
    }
    my $res=$bunshi/$bunbo;
    my $cap=$rdata{"CPU"};
    my $row="\"".$cap."\"\t".$rdata{"Year"}."\t".$res."\n";
    print $fh $row;
  }
  # close arch files
  for my $key (keys %archfh) {
    close $archfh{$key};
  }
  # generate gnuplot plot command
  genplot(keys %archfh);
}

my $numarg=$#ARGV+1;
if($numarg==1) {
  my ($title,$sheet)=read_benchsrc($ARGV[0]);
  datfy($title,$sheet);
} else {
  print STDERR $descr;
}

exit 0;
